import os
import time
import threading
from threading import Thread
import subprocess as sub
import datetime
import random

class Sniffer:
    def __init__(self, scanDevice, pushFunction, test):
        self.scanDevice=scanDevice
        self.pushFunction=pushFunction
        self.stopSignal=False
        self.test = test
        self.thread = None
        pass

    def start(self):
        self.stopSignal=False
        self.thread = Thread(target=self.__run__)
        self.thread.start()
        pass

    def stop(self):
        with threading.Lock() as lock:
            self.stopSignal=True
            self.thread.join()
        pass

    def mac_addr(self, address):
        return ':'.join('%02x' % ord(b) for b in address)

    def buildMessage(self, bleSniffLine):

        split = bleSniffLine.split(' ')
        if len(split) < 4:
          print("invalid line <4")
          print(bleSniffLine)
          return None

        uuid = split[0]
        minor = split[1]
        major = split[2]
        signalStrenght = split[3]

        return {
            "uuid": uuid,
            "ma": major,
            "mi": minor,
            "ss": signalStrenght,
            "v": "1"
        }


    def __run__(self):
        print "Starting dump on: ", self.scanDevice
        #cmd = './ibeacon_scan -i ' + self.scanDevice + ' -b'
        cmd = './ibeacon_scan -b'
        print "COMMAND: " + cmd
        pc = sub.Popen(cmd, shell=True, stdout=sub.PIPE)
        print "Start sniffing..."
        for el in iter(pc.stdout.readline, b''):
            try:
                line = el.rstrip()
                res = self.buildMessage(line)
                print(str(res))
                #print line
                if self.test:
                    if res!=None:
                        print res
                    else:
                        print "SKIP"
                else:
                    if res != None:
                        self.pushFunction(res)
                        #for i in range(0, 100):
                            #self.pushFunction({
                            #  "st": 4,
                            #  "ma": self.rand_mac(),
                            #  "si": "",
                            #  "ss": -42,
                            #  "pr": -1
                            #})
            except Exception as e:
                print e
                pass

            with threading.Lock() as lock:
                if self.stopSignal:
                    print "Received stop-signal"
                    break
        pass

        def rand_mac(self):
            return "%02x:%02x:%02x:%02x:%02x:%02x" % (
                random.randint(0, 255),
                random.randint(0, 255),
                random.randint(0, 255),
                random.randint(0, 255),
                random.randint(0, 255),
                random.randint(0, 255)
                )
