#!/usr/bin/python
import os
import sys
import threading
import time
from pyshare import PyShare, PyShareProducer
from sniffer import Sniffer
from time import gmtime, strftime

scanDevice = os.getenv('SCAN_DEVICE', "hci0")

test = False
if len(sys.argv)>1:
  test = sys.argv[1]=="TEST_MODE"

class DataProducer(PyShareProducer):
  def __init__(self, scanDevice, test=False):
    self.sniffer = Sniffer(scanDevice, self.__push__, test)
    self.running = False
    pass

  def close(self):
    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print "Closing DataProducer"
    with threading.Lock() as lock:
      self.sniffer.stop()
      self.running = False
    pass

  def start(self):
    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print "Init DataProducer"
    with threading.Lock() as lock:
        if not self.running:
          self.running = True
          self.sniffer.start()
          pass

    #obj = {"ma": "00:00:00:00:00:", "si": "", "ss": -128, "st": 4, "pr": 1}
    #Since this object extends PyShareProducer we can access the queue like this
    #self.__push__(obj)

os.system("mkdir -p /opt/ble-sniffer/tmp/")

#Create a new pyshare object to handle 'ble-beacons' in a queue located at './tmp/queue'

t = time.time()
share = PyShare("ble_beacons", "/opt/ble-sniffer/tmp/ble_beacons.queue_" + str(t))

if os.path.isfile(share.deviceidlocation+"scandevice"):
    with open(share.deviceidlocation+"scandevice", 'r') as file:
        scanDevice = file.read().split("\n")[0]

#Define the data, use SQLITE datatypes
#tm and id are automatically injected by pyshare
share.addFieldToDefinition("uuid", "TEXT")
share.addFieldToDefinition("ma", "INTEGER") #major
share.addFieldToDefinition("mi", "INTEGER") #minor
share.addFieldToDefinition("ss", "INTEGER") #signal strength
share.addFieldToDefinition("v", "TEXT") # version
share.addCreationIndexClause("CREATE INDEX if not exists bucketMa ON pyshare_ble_beacons (ma, tm);")

#TTL in minutes, after this expires without getting any data the producer is re-booted
share.setTTL(1)

#Webservice url, where to store data (absolute path will not be edit anyway)
#http://in.mmma.cloud/ingest/:dataType/:deviceId/
#:deviceId/ will be injected automatically
share.setWebservice("http://in.iottacle.com/ingest/ble-beacons/")
#share.setWebservice("http://192.168.178.46:8080/ingest/netdata/")
#Set the sqlite db name
share.setDb("/opt/ble-sniffer/tmp/ble_beacons.sqlite")

#Set the producer object
share.setProducerFunction(DataProducer(scanDevice, test=test))

#Only for testing:
#means run 5 iterations, do not set this parameter to let the program run forever
if test:
    share.run = 5

#Start PyShare
share.__run__()

#If something fails close it
share.close()
