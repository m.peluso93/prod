# BLE sniffer

Building the image

    docker build -t ble-sniffer .

Running the image in TEST_MODE on hci0

    docker run \
	--name netclient \
	-d \
	-e "SCAN_DEVICE=hci0" \
	-v /dev/hci0:/dev/hci0
	--privileged \
        ble-sniffer
