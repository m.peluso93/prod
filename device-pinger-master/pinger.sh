#!/bin/bash

VER=$(python2.7 --version 2>&1)
echo "VER:"
echo "$VER"

SSH_KEY_FILE="/run/k"
SERVER_PORT="80"
#DEVICE_ID=$(cat /opt/common/device_id)

USERNAME=remotedevice
PASSWORD=RemDevRC120akk

AUTH_KEY=$(curl http://pinga.iottacle.com/api/login/login -fs -m 30 --insecure -XPOST -H 'Content-Type: application/json' -d '{"username": "'$USERNAME'", "password":"'$PASSWORD'"}' | python2.7 -c "import sys, json; print json.load(sys.stdin)['authKey']")

echo $AUTH_KEY

RESPONSE=$(curl -fs -m 30 --insecure -XPOST -H 'auth-token: '$AUTH_KEY'' -H "Content-type: application/json" -d '{
  "pingId" : 4,
  "pingerVersion" : 1,
  "deviceId": "'$DEVICE_ID'",
  "deviceTime" : 1234,
  "command" : "DO_PING_COMMAND" ,
  "previousPingTimeToFly" : 12345,
  "previousPingId" : 4
}' http://ping.iottacle.com/api/executor/command/c)

#/bin/echo "RESPONSE START"

#/bin/echo $RESPONSE

#/bin/echo "RESPONSE END"


if [ -z "$RESPONSE" ]; then
    echo "empty response"
    return 0
fi

COMP_VER=$(/bin/echo $RESPONSE | python2.7 -c "import sys, json, os; print json.load(sys.stdin)['compatibleVersions']")
echo "compatible versions:"
echo $COMP_VER

COMMAND=$(/bin/echo $RESPONSE | python2.7 -c "import sys, json; print json.load(sys.stdin)['command']")
echo "extracted command:"
echo $COMMAND

if [ -z "$COMMAND" ]; then
    echo "empty command"
elif [ "$COMMAND" = "OPEN_SSH" ];
then
    CMD_ID=$(/bin/echo $RESPONSE      | python2.7 -c "import sys, json; print json.load(sys.stdin)['commandId']")
    SSH_PORT=$(/bin/echo $RESPONSE    | python2.7 -c "import sys, json; print json.load(sys.stdin)['sshPort']")
    PEM_KEY=$(/bin/echo $RESPONSE     | python2.7 -c "import sys, json; print json.load(sys.stdin)['pemKey']")
    SSH_USER=$(/bin/echo $RESPONSE    | python2.7 -c "import sys, json; print json.load(sys.stdin)['sshUser']")
    SSH_ADDRESS=$(/bin/echo $RESPONSE | python2.7 -c "import sys, json; print json.load(sys.stdin)['sshAddress']")
    sleep 5
    #say to server that task is executing
    CMD_REPLY=$(curl -fs -m 30 --insecure -XPOST -H 'auth-token: '$AUTH_KEY'' -H "Content-type: application/json" -d '{
      "command" : "COMMAND_REPLY",
      "deviceId": "'$DEVICE_ID'",
      "commandId" : "'$CMD_ID'",
      "status": "EXECUTING",
      "content": ""
    }' http://ping.iottacle.com/api/executor/command/c)

    echo "$PEM_KEY" > "$SSH_KEY_FILE"
    chmod 600 $SSH_KEY_FILE
    echo "requested to open ssh on port $SSH_PORT"
    ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -Nf -R $SSH_PORT:localhost:22222 -i  $SSH_KEY_FILE $SSH_USER@$SSH_ADDRESS
    sleep 5

    #say to server that task is executing
    CMD_REPLY=$(curl -fs -m 30 --insecure -XPOST -H 'auth-token: '$AUTH_KEY'' -H "Content-type: application/json" -d '{
      "command" : "COMMAND_REPLY",
      "deviceId": "'$DEVICE_ID'",
      "commandId" : "'$CMD_ID'",
      "status": "END",
      "content": "'$CMD_REPLY'"
    }' http://ping.iottacle.com/api/executor/command/c)
    echo "ssh tunnel opened"

    CMD_REPLY=$(curl -fs -m 30 --insecure -XPOST -H 'auth-token: '$AUTH_KEY'' -H "Content-type: application/json" -d '{
      "command" : "COMMAND_REPLY",
      "deviceId": "'$DEVICE_ID'",
      "commandId" : "'$CMD_ID'",
      "status": "END_EXECUTION",
      "content": "'$CMD_REPLY'"
    }' http://ping.iottacle.com/api/executor/command/c)
else
    echo "command $COMMAND not recognized"

fi
