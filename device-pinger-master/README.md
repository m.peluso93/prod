Container che abilita un cron job che manca un ping al server ogni minuto
permette di attivare una connessione ssh senza che il dispositivo sia
connesso alla rete locale

Files:

    Dockerfile: crea /opt/pinger /opt/common, definisce il messaggio da inviare
                al server in device_id, sposta gli eseguibili
           
    setup.sh: assegna lo script pinger.sh al cronjob */1 * * * * significa esegui
              il cronjob ogni minuto 
              cronjob # minuti ore  giorni mesi giorni_della_settimana   command
        
    pinger.sh: invia un ping al server e rimane in ascolto nel caso venga richiesta
               una connessione ssh
               
            
            
TODO:

    Modificare il file pinger.sh in modo tale che non utilizzi python2.7 e 
    che possa ascoltare altri comandi oltre all'apertura del tunnel sshß