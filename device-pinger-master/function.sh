#!/bin/bash 

elif [ "$COMMAND" = "TROUBLESHOOT" ];
then
    
    dev="$(nmcli d | grep -E 'connected')"   #NetworkManager Devices connected and disconnected
    modem="$(mmcli -m 0)"                    #Modem Manager information about 3gDongle
    crons="$(journalctl -u cron.service)"    #Information about CRON SERVICE
    radio="$(nmcli radio all)"               #Information about all the internet connection
    interface="$(iw dev | grep Interface)"   #Information about the name interfaces
    
    CMD_REPLY=$(curl -fs -m 30 --insecure -XPOST -H 'auth-token: '$AUTH_KEY'' -H "Content-type: application/json" -d '{
      "command" : "TROUBLESHOOT-REPLY",
      "deviceId": "'$DEVICE_ID'",
      "App-ID": "$BALENA_APP_ID",
      "commandId" : "'$CMD_ID'",
      "content": "'$CMD_REPLY'",
      "dev":"'$dev'",
      "modem":"'$modem'",
      "crons":"'$crons'",
      "radio":"'$radio'",
      "interface":"'$interface'"
    }' http://ping.iottacle.com/api/executor/command/c)

elif [ "$COMMAND" = "INQUIRE" ];
then
    
    CMD_REPLY=$(curl -fs -m 30 --insecure -XPOST -H 'auth-token: '$AUTH_KEY'' -H "Content-type: application/json" -d '{
      "command" : "INQUIRE-REPLY",
      "deviceId": "'$DEVICE_ID'",
      "commandId" : "'$CMD_ID'",
      "content": "'$CMD_REPLY'",
      "BALENA_DEVICE_UUID": "'$BALENA_DEVICE_UUID'",
      "BALENA_APP_ID": "'$BALENA_APP_ID'",
      "BALENA_APP_NAME": "'$BALENA_APP_NAME'",
      "BALENA_APP_ID": "'$BALENA_APP_ID'"
    }' http://ping.iottacle.com/api/executor/command/c)