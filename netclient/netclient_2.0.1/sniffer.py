#!/usr/bin/python3
# coding=utf-8
import threading
import logging
import os
import time
import binascii
import datetime
import random
import subprocess as sub
from threading import Thread


class Sniffer:
    """
        Class Sniffer:
            This class takes a input the chosen scanDevice to perform the
            action of sniffer.
            A wifi-sniffer is ... TODO!!
        Ref: http://home.deib.polimi.it/redondi/WI/Sniffing_WiFi.pdf
        Ref: https://networkengineering.stackexchange.com/questions/25100/four-layer-2-addresses-in-802-11-frame-header
        Inputs:
            - ScanDevice
            - pushFunction
            - test
        Example:
            ---- todo ---
    """

    def __init__(self, scanDevices, pushFunction, test):
        """
                Initialization of the sniffer class
                Inputs:
                    - scanDevice:   chosen device to perform the sniffing
                    - pushFunction: push function from pyshare
                    - test:         flag
        """
        self.scanDevices = scanDevices
        self.pushFunction = pushFunction
        self.stopSignal = False
        self.test = test
        self.thread = None
        #        logging.basicConfig(level=logging.DEBUG)
        pass

    def enableMonitorMode(self):
        """
            Function which activate the Monitor Mode of a chosen Wifi device
        """
        os.system("ip link set %s down" % self.scanDevice)
        os.system("iwconfig %s mode monitor" % self.scanDevice)
        os.system("ip link set %s promisc on" % self.scanDevice)
        os.system("ip link set %s up" % self.scanDevice)

    def start(self):
        """
           Function which start the process of sniffing
        """
        self.stopSignal = False
        self.thread = Thread(target=self.__run__)
        self.thread.start()

    pass

    def stop(self):
        """
                    Function which stop the process of sniffing
        """
        with threading.Lock() as lock:
            self.stopSignal = True
            self.thread.join()
        pass

    def mac_addr(self, address):
        """Convert a MAC address to a readable/printable string
       Args:
         address (str): a MAC address in hex form (e.g. '\x01\x02\x03\x04\x05\x06')
       Returns:
         str: Printable/readable MAC address
        """
        return ':'.join('%02x' % ord(b) for b in address)

    def rand_mac(self):
        """
            Function which generates a random MAC ADDRESS
            Returns:
                - mac address
        """
        return "%02x:%02x:%02x:%02x:%02x:%02x" % (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255)
        )

    def buildMessage(self, tcpDumpLine):
        """
            Function which  parse the incoming line from tcpdump and builds the message to be sent to the database
            Example of tcpdump output:
                12:24:17.548442 1.0 Mb/s 2437 MHz 11b -79dBm signal antenna 1 0us BSSID:Broadcast DA:Broadcast \
                    X:X:X:X:X:X:X (oui Unknown) Probe Request (Next14) [1.0 2.0 5.5 11.0 Mbit]
            Inputs:
                - tcpDumpLine: Line read from the standard output of the tcp_dump command
            Returns:
                - T
        """
        # 0 False, 1 True --> if 1 (True) send the messages with the randomized mac addresses
        flag_mac_rand = None
        send_rand = os.getenv('SEND_RAND', '1')
        splitted = tcpDumpLine.split(' ')

        if len(splitted) < 4:
            logging.debug("invalid line <4")
            return None
        if splitted[4] != 'MHz':
            logging.debug("invalid line: " + splitted[4])
            # naive but fast way to see if the line is a good line or not
            return None
        offsetFix = 0  # used to fix offset in case of driver is 5370 instead of 7601
        try:
            if splitted[17] == 'Request':
                offsetFix = 2
            else:
                index = splitted.index("Request")
        except Exception as e:
            logging.debug('Exception ' + e + ' Handling of request exception')
            return None
        try:
            if 'Request' not in splitted:
                logging.debug("invalid line, probe response, not dealing with it now")
                logging.debug(tcpDumpLine)
                return None
        except Exception as e:
            logging.debug('Exception ' + e)
            return None

        time1 = splitted[0].split(':')
        secs = time1[2].split('.')
        naive = datetime.datetime.now()
        naive.replace(hour=int(time1[0]), minute=int(time1[1]), second=int(secs[0]), microsecond=int(secs[1]))
        eventTime = time.mktime(naive.timetuple()) * 1000

        freq = 0
        try:
            if splitted[3] != "2412":
                freq = 1
        except Exception as e:
            logging.debug('Exception ', e)
            return None

        signalStrenght = int(splitted[6][0:splitted[6].find('d')])

        if offsetFix == 2:
            endmac = 12 + offsetFix - 1
            index = splitted.index("Request")
        else:
            endmac = 12 + (index - 12 - 1)

        try:
            mac = splitted[endmac].split(":")
            mac = mac[1] + ":" + mac[2] + ":" + mac[3] + ":" + mac[4] + ":" + mac[5] + ":" + mac[6]
            ssid = splitted[index + 1]
        except Exception as e:
            logging.debug('Exception ', e)
            return None
        flag_mac_rand = self.check_if_rand(mac)

        if send_rand == '1':
            return {"st": 4, "ma": mac, "si": ssid, "ss": signalStrenght, "pr": 3, 'rd': flag_mac_rand, 'tm': eventTime}
        elif send_rand == '0' and flag_mac_rand == "false":
            return {"st": 4, "ma": mac, "si": ssid, "ss": signalStrenght, "pr": 3, 'rd': flag_mac_rand, 'tm': eventTime}
        else:
            return None

    def check_if_rand(self, mac):
        """
            Function which identify if an input mac address is random or true

            Inputs:
                - Mac Address

            Returns:
                - Flag
        """
        if mac[1] in ['2', '3', '6', '7', 'A', 'B', 'E', 'F', 'a', 'b', 'e', 'f']:
            return "true"
        else:
            return "false"

    def generate_cmd(self):
        """
          Function which generate the input command for tcpdump:
          Options:
            -i: Listen on interface. If unspecified, tcpdump searches the system interface list.
            -e: Print the link-level header on each dump line.
            -s: Snarf snaplen bytes of data from each packet rather than the default of 262144 bytes (default 0).
            -v: Slightly more verbose output, for even more verbose -vvv
            -l: Make stdout line buffered.
            --monitor-mode: Put the interface in "monitor mode";
            probe-req:
        """

        cmd = '/usr/sbin/tcpdump -i ' + self.scanDevice + ' -v -e -s 0 -l --monitor-mode type mgt subtype probe-req'
        logging.info("COMMAND: " + cmd)
        return cmd

    def stream(self, cmd):
        """
          Function which start the sniffing stream of information and push the message through
          pyShare to the database listening at the port:  xxxxxx
        """
        self.pc = sub.Popen(cmd, shell=True, stdout=sub.PIPE, stderr=sub.PIPE)
        for stdout in iter(self.pc.stdout.readline, b''):
            try:
                l_stdout = stdout.rstrip()
            except Exception as e:
                logging.debug('Exception ' + e)
                return False

            if 'interface went down' in str(l_stdout):
                logging.debug('stderr: ' + str(l_stdout))
                return False

            l_stdout = l_stdout.decode()
            res = self.buildMessage(l_stdout)

            if res != None:
                self.pushFunction(res)

            with threading.Lock() as lock:
                if self.stopSignal:
                    logging.debug("Received stop-signal")
                    break
        return True

    def __run__(self):
        """
            Function which execute and handle the streaming and his exceptions
        """
        condition = True
        condition_stream = True

        for self.scanDevice in self.scanDevices:
            logging.info("Starting tcpDump on: " + str(self.scanDevice))
            cmd = self.generate_cmd()

            logging.info("Enabled Monitor Mode")
            self.enableMonitorMode()

            while condition:
                while condition_stream:
                    condition_stream = self.stream(cmd)

                self.pc.terminate()
                logging.info('Reset ---> Done')
                condition_stream = True
        pass