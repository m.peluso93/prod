# Network scanner - client

### Dependencies:

*   tcpdump 
*   wireless-tools
*   wpasupplicant
*   dnsmasq

apt-get install -y tcpdump wireless-tools wpasupplicant dnsmasq 

Then from pip repository

*   pip install -r requirements.txt


### Environmental variables:

SCAN_DEVICE if not set the code will loop over the possible wifi-interfaces 
SEND_RAND   1/0 if 1 send random mac addresses

### Building the image

balena build --deviceType raspberrypi3 --arch armv7hf --emulated --dockerfile Dockerfile

