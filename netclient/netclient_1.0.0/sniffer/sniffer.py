# coding=utf-8
import os
import time
import pcap, dpkt
import threading
from threading import Thread
import thread
import binascii
import subprocess as sub
import pytz, datetime
import random
import signal

class Sniffer:
  def __init__(self, scanDevices, pushFunction, test):
    self.scanDevices=scanDevices
    self.pushFunction=pushFunction
    self.stopSignal=False
    self.test = test
    self.thread = None
    pass

  def enableMonitorMode(self):
    os.system("ip link set %s down" % self.scanDevice)
    os.system("iwconfig %s mode monitor" % self.scanDevice)
    os.system("ip link set %s promisc on" % self.scanDevice)
    os.system("ip link set %s up" % self.scanDevice)

  def start(self):
    self.stopSignal=False
    self.thread = Thread(target=self.__run__)
    self.thread.start()

    pass

  def stop(self):
    with threading.Lock() as lock:
        self.stopSignal=True
        self.thread.join()
    pass

  def mac_addr(self, address):
    """Convert a MAC address to a readable/printable string

       Args:
         address (str): a MAC address in hex form (e.g. '\x01\x02\x03\x04\x05\x06')
       Returns:
         str: Printable/readable MAC address
    """
    return ':'.join('%02x' % ord(b) for b in address)

  def buildMessage(self, tcpDumpLine):
      #https://networkengineering.stackexchange.com/questions/25100/four-layer-2-addresses-in-802-11-frame-header
      #tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
      #listening on wlan1, link-type IEEE802_11_RADIO (802.11 plus radiotap header), capture size 262144 bytes
      #15:29:21.308165 1.0 Mb/s 2412 MHz 11b -85dBm signal -85dBm signal antenna 0 BSSID:Broadcast DA:Broadcast SA:e0:db:10:fa:33:7a (oui Unknown) Probe Request (Vodafone-33668070) [1.0 2.0 5.5 11.0 Mbit]
      #12:53:03.501637 1.0 Mb/s 2412 MHz 11b -89dBm signal antenna 1 BSSID:Broadcast DA:Broadcast SA:84:55:a5:b9:3e:6d (oui Unknown) Probe Request () [1.0 2.0 5.5 11.0 Mbit]
      #12:53:03.505146 1.0 Mb/s 2412 MHz 11b -79dBm signal antenna 1 BSSID:e0:91:f5:f6:68:ec (oui Unknown) DA:84:55:a5:b9:3e:6d (oui Unknown) SA:e0:91:f5:f6:68:ec (oui Unknown)
      #      Probe Response (Nico-PC-Wireless) [1.0* 2.0* 5.5* 11.0* 18.0 24.0 36.0 54.0 Mbit] CH: 1, PRIVACY
      #36 packets captured
      #37 packets received by filter
      #0 packets dropped by kernel
      #1 packet dropped by interface

      splitted = tcpDumpLine.split(' ')
      if len(splitted) < 4:
          print("invalid line <4")
          return None
      if splitted[4] != 'MHz':
          print("invalid line: " + splitted[4])
          #naive but fast way to see if the line is a good line or not
          return None

      offsetFix = 0 #used to fix offset in case of driver is 5370 instead of 7601
      if splitted[18] == 'Request':
          offsetFix = 2

      if splitted[16 + offsetFix] != 'Request':
          print(tcpDumpLine)
          print("invalid line, probe response, not dealing with it now")
          return None

      time1 = splitted[0].split(':')
      secs = time1[2].split('.')
      naive= datetime.datetime.now()
      naive.replace(hour=int(time1[0]), minute=int(time1[1]), second=int(secs[0]), microsecond=int(secs[1]))
      eventTime = time.mktime(naive.timetuple()) * 1000

      freq = 0
      if splitted[3] != "2412":
         freq = 1

      signalStrenght = int(splitted[6][0:splitted[6].find('d')])
      #print (signalStrenght)

      mac = splitted[12 + offsetFix].split(":")
      mac = mac[1] + ":" + mac[2] + ":" + mac[3] + ":" + mac[4] + ":" + mac[5] + ":" + mac[6]
      #print(mac)

      ssid = splitted[17 + offsetFix][1:len(splitted[17 + offsetFix])-1]
      #print(ssid)
      return {
        "st": 4,
        "ma": mac,
        "si": ssid,
        "ss": signalStrenght,
        "pr": 3
      }
      #return None


  def generate_cmd(self):
    """
      pancakes
    """

    #cmd = '/usr/sbin/tcpdump -i ' + self.scanDevice + ' -e -s 0 -l --monitor-mode type mgt subtype probe-resp or subtype probe-req'
    cmd = '/usr/sbin/tcpdump -i ' + self.scanDevice + ' -e -s 0 -l --monitor-mode type mgt subtype probe-req'
    print "COMMAND: " + cmd

    return cmd

  def stream(self,cmd):
    """
      pancakes
    """
    #print 'generate subprocess'
    self.pc = sub.Popen(cmd, shell=True, stdout=sub.PIPE,stderr=sub.PIPE)
    #print  'pc done'
    for stdout in iter(self.pc.stdout.readline, b''):
      #print 'started for'
      try:
        #print 'reading stream'
        l_stdout = stdout.rstrip()
        #print 'done reading ' + str(l_stdout)
      except Exception as e:
        print  e
        return False

      if 'interface went down' in l_stdout:
        print  'stderr: ' + str(l_stdout)
        return False

      #print l_stdout
      #print 'sending message'
      res = self.buildMessage(l_stdout)
      #print 'built message'
      #print 'res: ' + str(res)
      if self.test:
        if res!=None:
          print 'res: ' + str(res)
        else:
          print "SKIP"
      else:
        if res != None:
          # print 'before push'
          self.pushFunction(res)
          #print 'pushFunction done'


      with threading.Lock() as lock:
        if self.stopSignal:
          print "Received stop-signal"
          break

    return True



  def __run__(self):

    condition        = True
    condition_stream = True

    for self.scanDevice in self.scanDevices:
        print "Starting tcpDump on: ", self.scanDevice
        cmd = self.generate_cmd()

        print "Enabled Monitor Mode"
        self.enableMonitorMode()

        while condition:

          while condition_stream:
            condition_stream = self.stream(cmd)

          self.pc.terminate()
          print 'Reset ---> Done'

          condition_stream = True

    pass

  def rand_mac(self):
    return "%02x:%02x:%02x:%02x:%02x:%02x" % (
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255)
        )
