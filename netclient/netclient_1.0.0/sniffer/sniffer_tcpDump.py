import os
import pcap, dpkt
import threading
from threading import Thread
import thread
import binascii
import subprocess as sub

class Sniffer:
  def __init__(self, scanDevice, pushFunction, test):
    self.scanDevice=scanDevice
    self.pushFunction=pushFunction
    self.stopSignal=False
    self.test = test
    self.thread = None
    pass

  def enableMonitorMode(self):
    os.system("ip link set %s down" % self.scanDevice)
    os.system("iwconfig %s mode monitor" % self.scanDevice)
    os.system("ip link set %s promisc on" % self.scanDevice)
    os.system("ip link set %s up" % self.scanDevice)

  def start(self):
    self.stopSignal=False
    self.thread = Thread(target=self.__run__)
    self.thread.start()
    pass

  def stop(self):
    with threading.Lock() as lock:
        self.stopSignal=True
        self.thread.join()
    pass

  def mac_addr(self, address):
    """Convert a MAC address to a readable/printable string

       Args:
         address (str): a MAC address in hex form (e.g. '\x01\x02\x03\x04\x05\x06')
       Returns:
         str: Printable/readable MAC address
    """
    return ':'.join('%02x' % ord(b) for b in address)

  def buildMessage(self, ts, pkt, lnk):
      #print dir(pkt)
      #eth = dpkt.ethernet.Ethernet(pkt)
      if lnk==127:
        tap = dpkt.radiotap.Radiotap(pkt)
        signal=-(256-tap.ant_sig.db)    #Calculate signal strength
        t_len=binascii.hexlify(pkt[2:3])  #t_len field indicates the entire length of the radiotap data, including the radiotap header.
        t_len=int(t_len,16)
        #wlan = dpkt.ieee80211.IEEE80211(pkt)
        wlan = dpkt.ieee80211.IEEE80211(pkt[t_len:])
        ssid = None
        mac = None
        if wlan.type == 0 and wlan.subtype == 4: # Indicates a probe request
          #print `wlan`
          #print wlan.mgmt.src
          ssid = wlan.ies[0].info
          mac = self.mac_addr(wlan.mgmt.src)
          if wlan.type == 0 and wlan.subtype == 0:
            #print `wlan`
            mac = self.mac_addr(wlan.mgmt.src)
            #ssid = wlan.mgmt.bssid
          return {
            "st": wlan.subtype,
            "ma": mac,
            "si": ssid,
            "ss": signal,
            "pr": 3
          }
      return None

  def __run__(self):
      print "Starting tcpDump on: ", self.scanDevice
      pc = sub.Popen(('/usr/sbin/tcpdump', '-i '+self.scanDevice +' -v -e -s 0 -l --monitor-mode type mgt subtype probe-resp and subtype probe-req'))
      print "Start sniffing..."
      for el in iter(pc.stdout.readline, b''):
	print el.rstrip()
        #self.pushFunction(self.buildMessage(ts, pkt, pc.datalink()))

        with threading.Lock() as lock:
            if self.stopSignal:
              print "Received stop-signal"
              break

      pass
