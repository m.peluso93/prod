# StoreAndShare

This library will store locally data until the software is able to share it with a rest-server.

## Dependencies

To install dependencies use pip:

    sudo pip install -r requirements.txt

Non-Python dependencies:

    sudo apt-get install sqlite
