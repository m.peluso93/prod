#!/usr/bin/python
from __future__ import with_statement # 2.5 only
import threading
import sqlite3
import time
import os
import os.path
import traceback
from json import loads, dumps
from time import gmtime, strftime

#not python-standard deps
from queuelib import FifoDiskQueue
from queuelib import LifoDiskQueue
import requests

#Exception raised when performing operations not allowed during the run status
class AlreadyRunningException(Exception):
    pass

#PyShareProducer is an Abstract Singleton
class PyShareProducer:
    queue = None
    def __load__(self, queue):
        self.queue = queue

    def __push__(self, obj):
        obj["tm"]=int(round(time.time() * 1000))
        self.queue.push(dumps(obj))

    def close(self):
        pass

    def start(self):
        pass

#PyShare is a Singleton
class PyShare:
    deviceidlocation = "/etc/mmma/" #device_id / device_name
    maxBucketDimension = 60
    maxThresholdElement = 1000
    datatype = None
    definition = []
    elementToSendById = {}
    elementToSendByTime = {}
    globalCount = 0
    deviceid = ""
    devicename = ""
    database = None
    indexCreation = None
    persistentqueue = None
    producer = None
    lastdataproduced = 0
    TTL = 5 #minutes
    webserviceUrl = ""
    #slackUrl = "https://hooks.slack.com/services/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"
    slackUrl = "https://hooks.slack.com/services/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"

    run = -1 #Means forever
    running = False

    def __init__(self, datatype, queuefile):
        self.datatype = datatype
        self.persistentqueue = FifoDiskQueue(queuefile, chunksize=10)
        self.definition.append({"key": "id", "def": "INTEGER PRIMARY KEY AUTOINCREMENT", "special": True, "default": "NULL"})
        self.definition.append({"key": "tm", "def": "INTEGER", "special": False})

        self.deviceid = "-1000"
        self.devicename = "TestDevice"
        if os.path.isfile(self.deviceidlocation+"device_id"):
            with open(self.deviceidlocation+"device_id", 'r') as file:
                self.deviceid = file.read().split("\n")[0]
                self.devicename = self.deviceid
        if os.path.isfile(self.deviceidlocation+"device_name"):
            with open(self.deviceidlocation+"device_name", 'r') as file:
                self.devicename = file.read().split("\n")[0]
        pass

    def addFieldToDefinition(self, key, type):
        if self.running:
            raise AlreadyRunningException("PyShare is running, cannot change the definition");
        self.definition.append({"key": key, "def": type, "special": False})

    def addCreationIndexClause(self, clause):
	self.indexCreation = clause

    def setProducerFunction(self, function):
        if self.running:
            raise AlreadyRunningException("PyShare is running, cannot change the producer function");
        function.__load__(self)
        self.producer = function

    def setDb(self, dbname):
        self.database = {"name": dbname};
        pass

    def setWebservice(self, url):
        self.webserviceUrl = url

    def setDeviceId(self, lbl, id):
        self.devicename = lbl
        self.deviceid = id

    def setTTL(self, ttl):
        if self.running:
            raise AlreadyRunningException("PyShare is running, cannot change the producer function");
        self.TTL = ttl;

    def push(self, obj):
        with threading.Lock() as lock:
            self.persistentqueue.push(obj)
        pass

    def __run__(self):
        if self.running:
            raise AlreadyRunningException("Pyshare is already running")
        try:
            self.running = True
            # 0. connect to db, and create schema if not exists
            self.__notify__("Initializing")
            self.__db_create__()
            self.producer.start()
            self.__notify__("Started")

            anyDataArrived = False
            last = time.time()
            lastnotify = time.time()
            count = 0
            while (self.run<0 or self.run>0):
                if self.run>0:
                    self.run-=1
                # 1. while true dequeue and put in db
                emptyQueue = False
		cnt = 0
		q = []
                with threading.Lock() as lock:
                    while (not emptyQueue): #for i in range(0, 100):
    		        cnt = cnt + 1
			#print("sucking from persistent queue " + str(cnt))
                        obj = None
                        el = self.persistentqueue.pop()
                        if el==None:
			    #print("el = none")
			    break #emptyQueue=True
                        else:
			    #print("el = " + str(el))
                            obj = loads(str(el))
                            if not anyDataArrived:
                                anyDataArrived=True
                                last = obj["tm"]
                            if obj["tm"]>last:
                                last = obj["tm"]
                            if obj!=None:
                                q.append(obj)#self.__db_insert__(obj)
                            else:
			        #print("empty queue")
                                emptyQueue=True
                for i in range(0, len(q)):
                    #print("db insert: " + str(q[i]))
                    self.__db_insert__(q[i])
                # 2. grab oldest N data not sent, and send them online
                data = self.__db_get_latest__(200)
		#print("sending data " + str(data))
                if len(data)>0:
                    ack = self.__send_data__(data)
                    count+=len(ack)
                    # 3. wait for response, delete only elements with ack.
                    self.__db_delete__(ack)
                # 4. check for service liveliness
                if emptyQueue and last-time.time()>self.TTL*60:
                    self.__service_reboot__()
                    anyDataArrived = False
                    last = time.time()
                    lastnotify = time.time()
                    count=0
                elif lastnotify-time.time()>=3600:
                    lastnotify = time.time()
                    count=0
                    self.__notify__("I'm still alive, in the last hour I've sent "+str(count)+" messages")
		time.sleep(1)
        except (KeyboardInterrupt, SystemExit):  # when you press ctrl+c
            print strftime("%Y-%m-%d %H:%M:%S", gmtime())
	    print "User kill signal"
            self.close()
        except Exception as e:
	    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
            print "Exception accurred", e
	    traceback.print_exc()
            self.__notify__("Closing due to error: "+str(e))
            self.close()
        pass

    def __db_create__(self):
        conn = sqlite3.connect(self.database["name"])
        self.database["conn"] = conn

        sql = "CREATE TABLE IF NOT EXISTS pyshare_"+self.datatype+" (";
        for i in range(0, len(self.definition)):
            if i>0:
                sql+=", "
            sql+=self.definition[i]["key"]+" "+self.definition[i]["def"]

        sql+=")"
        #print sql
        c = conn.cursor()
        c.execute(sql)
	if self.indexCreation is not None:
	    c = conn.cursor()
	    c.execute(self.indexCreation)

        conn.commit()

        pass

    def __db_insert__(self, obj):
        #conn = self.database["conn"];

	try:
	    if obj["ma"].startswith("da:a1:19"):
		return
	    ######
    	    #sql = "Select count(*) cnt  from pyshare_"+self.datatype
	    #c = conn.cursor()
 	    #c.execute(sql)
	    #count = c.fetchone()
	    ######
	    count = [len(self.elementToSendById)]
	    ######
	    currentBucketDim = float(self.maxBucketDimension) / self.maxThresholdElement * float(count[0])
            currentBucketDim = self.maxBucketDimension if currentBucketDim > self.maxBucketDimension else int(currentBucketDim)
	    currentBucketDim = 1 if currentBucketDim < 1 else int(currentBucketDim)

	    tm = int(obj['tm']/1000)
            tm = tm - (tm % currentBucketDim)
	    tm = tm * 1000
	    obj['tm'] = tm
	    ma = obj['ma']
	    obj['pr'] = currentBucketDim
	    obj['st'] = count[0]

	    #######
	    #sql = "select count(*) cnt  from pyshare_" + self.datatype + " where ma = \'"+ ma + "\' and tm = " + str(tm)
	    #c.execute(sql)
	    #count1 = c.fetchone()
	    ########
	    count1 = [0]
	    elementsInTm = str(tm) in self.elementToSendByTime
	    if elementsInTm is False:
		count1 = [0]
	    else:
		count1 = 0
		elementsInTm = self.elementToSendByTime[str(tm)]
		if obj['ma'] in elementsInTm:
		    count1 = 1
		count1 = [count1]
	    #######

	    #print ("time:" + str(obj['tm']) + " newTime: " + str(tm) + " totalCount:" + str(count[0]) + " bucketDim " + str(currentBucketDim)) + " elementsForMa: " + obj['ma'] + " = " + str(count1[0])

	    if (count1[0] == 0):
		##########
                #data = []
                #sql = "INSERT INTO pyshare_"+self.datatype+" VALUES (";
                #for i in range(0, len(self.definition)):
                #    if i>0:
                #        sql+=","
                #    if self.definition[i]["special"]:
                #        sql+=self.definition[i]["default"]
                #    else:
                #        sql+="?"
                #        data.append(obj[self.definition[i]["key"]])
                #sql+= ")"
                #print sql, data
                #c = conn.cursor()
                #c.execute(sql, data)
                #conn.commit()
		###########
                id = self.globalCount + 1
		self.globalCount = id
		obj['id'] = id
		elementById = id in self.elementToSendById
		if elementById is False:
		    self.elementToSendById[id] = [obj]
		else:
		    self.elementToSendById[id].append(obj)

		elementByTime = str(tm) in self.elementToSendByTime
		if elementByTime is False:
		    self.elementToSendByTime[str(tm)] = {}
		self.elementToSendByTime[str(tm)][obj['ma']] = obj
		###########
	except Exception, err:
	    traceback.print_exc()
	    raise err
        pass

    def __db_get_latest__(self, limit = 100):
        #conn = self.database["conn"];
        #data = []

	#sql = "select min(tm) mn from pyshare_"+self.datatype
	#c = conn.cursor()
        #c.execute(sql)
        #min = c.fetchone()[0]

        #sql = "SELECT * FROM pyshare_"+self.datatype+" ORDER BY tm DESC LIMIT "+str(limit)
        #c = conn.cursor()
        #for row in c.execute(sql):
        #    obj = {}
        #    for i in range(0, len(self.definition)):
        #        lbl = self.definition[i]["key"]
        #        val = row[i]
        #        if "INTEGER" in self.definition[i]["def"]:
        #            val = int(val)
        #        elif "REAL" in self.definition[i]["def"]:
        #            val = float(val)
        #        obj[lbl] = val
	#    obj["df"] = obj["tm"] - min
        #    data.append(obj)

        #return data
	length = len(self.elementToSendByTime)
	if length == 0:
	    return []

	elementsByTimeTmp = sorted(self.elementToSendByTime)
	elementsByTime = []
	for i in range(0, len(elementsByTimeTmp)):
	    elementsByTime.append(self.elementToSendByTime[elementsByTimeTmp[i]])

	min = long(float(elementsByTimeTmp[0]))
	data = []
	moreData = True
	while (len(data) < limit and moreData):
	    #print("range" + str(reversed(range(0, len(elementsByTime)))))
	    for i in reversed(range(0, len(elementsByTime))):
	        elementInSpecificTime = elementsByTime[i]
		#print ("getting element: " + str(i))
		#print ("len:" + str(len(elementInSpecificTime)))
		totCount = 0
	        for x in elementInSpecificTime:
		    totCount = totCount + 1
		    if 'stm' in elementInSpecificTime[x] and elementInSpecificTime[x]['stm'] is not None and elementInSpecificTime[x]['stm'] > time.time() * 1000 - 60000:
			#print ("skipping: " + str(elementInSpecificTime[x]['id']))
			pass
		    else:
		        obj = {}
       		        for io in range(0, len(self.definition)):
        		    lbl = self.definition[io]["key"]
			    #print(elementsByTime)
			    #print(elementInSpecificTime)
			    #print(elementInSpecificTime[x])
			    #print (lbl)
        		    val = elementInSpecificTime[x][lbl]
        		    if "INTEGER" in self.definition[io]["def"]:
        		        val = int(val)
        		    elif "REAL" in self.definition[io]["def"]:
        		        val = float(val)
        		    obj[lbl] = val
        	        obj["df"] = obj["tm"] - min
			obj['stm'] = time.time() * 1000
			#print("appending: " + obj['ma'] + " - " + str(obj['tm']) + " len:" + str(len(elementInSpecificTime[x])))
        	        data.append(obj)
		        if len(data) == limit:
			    moreData = False
		            break
		if len(data) == limit or i == 0:
		    moreData = False
                    break
	return data

    def __db_delete__(self, ids):
        #if len(ids)>0:
        #    conn = self.database["conn"];
        #    sql = "DELETE FROM pyshare_"+self.datatype+" WHERE id IN ("
        #    for i in range(0, len(ids)):
        #        if i>0: sql+=", "
        #        sql+=str(ids[i])
        #    sql+=")"
        #    ##print sql
        #    c = conn.cursor()
        #    c.execute(sql)
        #    conn.commit()
        #pass
	if len(ids)>0:
	    for i in range(0, len(ids)):
	        elementById = ids[i] in self.elementToSendById
		if elementById is True:
		    elementById = self.elementToSendById[ids[i]]
		    elementsByTime = str(elementById[0]['tm']) in self.elementToSendByTime
		    if elementsByTime is True:
			elementsByTime = self.elementToSendByTime[str(elementById[0]['tm'])]
			for x in elementsByTime:
			    if elementsByTime[x]['id'] == ids[i]:
				del elementsByTime[x]
				if len(elementsByTime) == 0:
			    	    del self.elementToSendByTime[str(elementById[0]['tm'])]
		    		del self.elementToSendById[ids[i]]
				break
	pass

    def __send_data__(self, data):
        device_id = os.getenv('DEVICE_ID', '-1000')
        (sts, res) = self.__post__(self.webserviceUrl+device_id, data)
        if sts>=200 and sts<299:
            return res
	print strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print "[WARNING] Cannot send data to", self.webserviceUrl+self.deviceid, "response code:", sts, "body:",res
        return []

    def __service_reboot__(self):
	print strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print "[WARNING]", "service is not producing data by", self.TTL, "minutes"
        self.producer.close()
        self.producer.start()
        self.__notify__("Service reboot")
        pass

    #Notifies a webhook using a post request
    def __notify__(self, msg):
        gravity = "[INFO]"
        if self.slackUrl!=None:
            try:
                os.system("curl -X POST --data-urlencode 'payload={\"username\": \""+self.devicename+" (pyshare "+self.datatype+")\", \"text\": \""+msg+"\"}' "+self.slackUrl)
            except:
		print strftime("%Y-%m-%d %H:%M:%S", gmtime())
                print "[WARNING]", "Cannot send message to slack"
        else:
            gravity = "[WARNING]"
	print strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print self.devicename+" (pyshare "+self.datatype+")", gravity, msg
        pass

    def __post__(self, url, payload):
        try:
            ##print dumps(payload)
            r = requests.post(url, data = dumps(payload), timeout=10)
            if r.status_code<299:
                return (r.status_code, loads(r.text))
            else:
                return (r.status_code, r.text)
        except requests.exceptions.ConnectionError as e:
            return (-1, "Connection error")
        except:
            return (0, "Unknown error!")

    def close(self):
        self.__notify__("Closing")
        self.producer.close()
        self.persistentqueue.close()
        if self.database!=None and self.database.has_key("conn"):
            self.database["conn"].close()
        self.__notify__("Closed")

    def __str__(self):
        txt = "PyShare "+ self.datatype
        txt+= "\n"
        return txt
