#!/usr/bin/python3
# coding=utf-8

import os
import time
import datetime
import random
import subprocess as sub
from   threading  import Thread

class Sniffer:
    """
        Class Sniffer:
            This class takes a input the chosen scanDevice to perform the
            action of sniffer.
            A wifi-sniffer is ... TODO!!

        Ref: http://home.deib.polimi.it/redondi/WI/Sniffing_WiFi.pdf
        Ref: https://networkengineering.stackexchange.com/questions/25100/four-layer-2-addresses-in-802-11-frame-header


        Inputs:
            - ScanDevice
            - pushFunction
            - test

        Example:
            ---- todo ---

    """
    def __init__(self, scanDevice, pushFunction, test):
        """
            Initialization of the sniffer class

            Inputs:
                - scanDevice:   chosen device to perform the sniffing
                - pushFunction: push function from pyshare
                - test:         flag
        """

        self.scanDevice    = scanDevice
        self.pushFunction  = pushFunction
        self.stopSignal    = False
        self.test          = test
        self.thread        = None

    def enableMonitorMode(self):
        """
            Function which activate the Monitor Mode of a chosen Wifi device

        """
        os.system("ip link set %s down"       % self.scanDevice)
        os.system("iwconfig %s mode monitor"  % self.scanDevice)
        os.system("ip link set %s promisc on" % self.scanDevice)
        os.system("ip link set %s up"         % self.scanDevice)

    def start(self):
        """
            Function which start the process of sniffing
        """
        self.stopSignal = False
        self.thread     = Thread( target=self.__run__ )
        self.thread.start()
        pass

    def stop(self):
        """
                    Function which stop the process of sniffing
        """
        with threading.Lock() as lock:
            self.stopSignal = True
            self.thread.join()
        pass

    def mac_addr(self, address):
        """
            Convert a MAC address to a readable/printable string

            Inputs:
                - address (str): a MAC address in hex form (e.g. '\x01\x02\x03\x04\x05\x06')

            Returns:
                - str: Printable/readable MAC address

        """
        return ':'.join('%02x' % ord(b) for b in address)

    def rand_mac(self):
        """
            Function which generates a random MAC ADDRESS

            Returns:
                - mac address

        """
        return "%02x:%02x:%02x:%02x:%02x:%02x" % (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255)
        )

    def buildMessage(self, tcpDumpLine):
        """
            Function which  parse the incoming line from tcpdump and builds the message to be sent to the database

            Example of tcpdump output:
                12:24:17.548442 1.0 Mb/s 2437 MHz 11b -79dBm signal antenna 1 0us BSSID:Broadcast DA:Broadcast \
                    X:X:X:X:X:X:X (oui Unknown) Probe Request (Next14) [1.0 2.0 5.5 11.0 Mbit]

            Inputs:
                - tcpDumpLine: Line read from the standard output of the tcp_dump command

            Returns:
                - T

        """
        verbose = False
        if '-v' in self.cmd: verbose = True

        splitted = tcpDumpLine.split(' ')

        if len(splitted) < 4:
            print("invalid line <4")
            return None

        if splitted[4] != 'MHz':
            print("invalid line: " + splitted[4])
            # naive but fast way to see if the line is a good line or not
            return None

        offsetFix = 0  # used to fix offset in case of driver is 5370 instead of 7601
        if splitted[18] == 'Request':
            offsetFix = 2

        if splitted[16 + offsetFix] != 'Request':
            print(tcpDumpLine)
            print("invalid line, probe response, not dealing with it now")
            return None

        time1     = splitted[0].split(':')
        secs      = time1[2].split('.')
        naive     = datetime.datetime.now()
        naive.replace(hour=int(time1[0]), minute=int(time1[1]), second=int(secs[0]), microsecond=int(secs[1]))

        eventTime = time.mktime(naive.timetuple()) * 1000

        freq = 0
        if splitted[3] != "2412":
            freq = 1

        signalStrenght = int(splitted[6][0:splitted[6].find('d')])

        mac = splitted[12 + offsetFix].split(":")
        mac = mac[1] + ":" + mac[2] + ":" + mac[3] + ":" + mac[4] + ":" + mac[5] + ":" + mac[6]

        ssid = splitted[17 + offsetFix][1:len(splitted[17 + offsetFix]) - 1]

        if verbose: print(
            "signalStrenght: " + str(signalStrenght) + "\n" + \
            "mac:"             + str(mac)            + "\n" + \
            "ssid:"            + str(ssid)           + "\n"
        )

        return {
            "st": 4,
            "ma": mac,
            "si": ssid,
            "ss": signalStrenght,
            "pr": 3
        }

    def generate_cmd(self):
        """
          Function which generate the input command for tcpdump:

          Options:
            -i: Listen on interface. If unspecified, tcpdump searches the system interface list.
            -e: Print the link-level header on each dump line.
            -s: Snarf snaplen bytes of data from each packet rather than the default of 262144 bytes (default 0).
            -v: Slightly more verbose output, for even more verbose -vvv
            -l: Make stdout line buffered.

            --monitor-mode: Put the interface in "monitor mode";
            probe-req:
        """

        self.cmd = '/usr/sbin/tcpdump -i ' + self.scanDevice + ' -v -e -s 0 -l --monitor-mode type mgt subtype probe-req'

        print("COMMAND: " + self.cmd)

    def stream(self):
        """
          Function which start the sniffing stream of information and push the message through
          pyShare to the database listening at the port:  xxxxxx

        """
        verbose = False
        if '-v' in self.cmd: verbose = True

        if verbose: print("Generate subprocess")

        self.pc = sub.Popen(self.cmd, shell=True, stdout=sub.PIPE, stderr=sub.PIPE)

        for stdout in iter(self.pc.stdout.readline, b''):
            try:
                if verbose: print('reading stream')
                l_stdout = stdout.rstrip()
                if verbose: print('done reading ' + str(l_stdout))

            except Exception as e:
                print("Exception while reading stream: " + str(e))
                return False

            if 'interface went down' in l_stdout:
                print('Interface went down --> stderr: ' + str(l_stdout))
                return False

            if verbose: print( l_stdout)
            res = self.buildMessage(l_stdout)
            if verbose: print('built message' + str(res))

            if self.test:
                if res != None:
                    print 'res: ' + str(res)
                else:
                    print "SKIP"
            else:
                if res != None:
                    self.pushFunction(res)

            with threading.Lock() as lock:
                if self.stopSignal:
                    print("Received stop-signal")
                    break

        return True

    def __run__(self):
        """
            Function which execute and handle the streaming and his exceptions

        """
        verbose = False
        if '-v' in self.cmd: verbose = True

        condition        = True
        condition_stream = True
        print ("Starting tcpDump on: " + str(self.scanDevice))

        # Generate command for Sniffer --> tcpdump
        self.cmd = self.generate_cmd()


        #TODO!! Aggiungere gestione degli scanDevice automaticamente su tutti i possibili wlan* (??)
        try:
            self.enableMonitorMode()
        except Exception as e:
            print("Exception while going on Monitor Mode: " + str(e))
            return -1
        print("Enabled Monitor Mode")


        while condition:

            while condition_stream:
                condition_stream = self.stream(self.cmd)
                if verbose: print('streaming: ' + str(condition_stream))


            self.pc.terminate()
            print('Reset streaming')
            condition_stream = True
        pass