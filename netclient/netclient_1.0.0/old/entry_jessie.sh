#!/bin/bash

set -m

if hostname "$HOSTNAME" &> /dev/null; then
	PRIVILEGED=true
else
	PRIVILEGED=false
fi

# Send SIGTERM to child processes of PID 1.
function signal_handler()
{
	kill "$pid"
}

function start_udev()
{
	if [ "$UDEV" == "on" ]; then
		if [ "$INITSYSTEM" != "on" ]; then
			if command -v udevd &>/dev/null; then
				unshare --net udevd --daemon &> /dev/null
			else
				unshare --net /lib/systemd/systemd-udevd --daemon &> /dev/null
			fi
			udevadm trigger &> /dev/null
		fi
	else
		if [ "$INITSYSTEM" == "on" ]; then
			systemctl mask systemd-udevd
		fi
	fi
}

function mount_dev()
{
	tmp_dir='/tmp/tmpmount'
	mkdir -p "$tmp_dir"
	mount -t devtmpfs none "$tmp_dir"
	mkdir -p "$tmp_dir/shm"
	mount --move /dev/shm "$tmp_dir/shm"
	mkdir -p "$tmp_dir/mqueue"
	mount --move /dev/mqueue "$tmp_dir/mqueue"
	mkdir -p "$tmp_dir/pts"
	mount --move /dev/pts "$tmp_dir/pts"
	touch "$tmp_dir/console"
	mount --move /dev/console "$tmp_dir/console"
	umount /dev || true
	mount --move "$tmp_dir" /dev

	# Since the devpts is mounted with -o newinstance by Docker, we need to make
	# /dev/ptmx point to its ptmx.
	# ref: https://www.kernel.org/doc/Documentation/filesystems/devpts.txt
	ln -sf /dev/pts/ptmx /dev/ptmx
	mount -t debugfs nodev /sys/kernel/debug
}

function init_systemd()
{
	GREEN='\033[0;32m'
	echo -e "${GREEN}Systemd init system enabled."
	for var in $(compgen -e); do
		printf '%q=%q\n' "$var" "${!var}"
	done > /etc/docker.env
	echo 'source /etc/docker.env' >> ~/.bashrc
        echo 'running resinApp.sh'
	printf '#!/bin/bash\n eval exec ' > /etc/resinApp.sh
	printf '%q ' "$@" >> /etc/resinApp.sh
	chmod +x /etc/resinApp.sh

        
	mkdir -p /etc/systemd/system/resin.service.d
	cat <<-EOF > /etc/systemd/system/resin.service.d/override.conf
		[Service]
		WorkingDirectory=$(pwd)
	EOF

        echo 'running env DBUS_SYSTEM_BUS_ADDRESS'

	exec env DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket /sbin/init systemd.show_status=0
        echo "waiting 1minute"
        sleep 1m
        echo "stopping NetworkManager"
        systemctl stop NetworkManager.service
        echo "NetworkManager disabled"

        echo "NetworkManager STATUS"
        systemctl status NetworkManager.service

        echo "ifconfig"
        ifconfig
        echo "sleep infinity"
	sleep infinity
}

function init_non_systemd()
{
	# trap the stop signal then send SIGTERM to user processes
	trap signal_handler SIGRTMIN+3 SIGTERM

	# echo error message, when executable file doesn't exist.
	if CMD=$(command -v "$1" 2>/dev/null); then
		shift
		"$CMD" "$@" &
		pid=$!
		wait "$pid"
		exit_code=$?
		fg &> /dev/null || exit "$exit_code"
	else
		echo "Command not found: $1"
		exit 1
	fi
}

INITSYSTEM=$(echo "$INITSYSTEM" | awk '{print tolower($0)}')

case "$INITSYSTEM" in
	'1' | 'true')
		INITSYSTEM='on'
	;;
esac

if $PRIVILEGED; then
	# Only run this in privileged container
        echo "running mount"
	mount_dev
        echo "running start_udev"
	start_udev
fi

if [ "$INITSYSTEM" = "on" ]; then
        echo "running init_systemd"
	init_systemd "$@"
else
        echo "running init_non_systemd"
	init_non_systemd "$@"
fi
