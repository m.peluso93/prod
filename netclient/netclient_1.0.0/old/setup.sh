#!/bin/sh
#sudo apt install -y python python-pip python-dev libpcap-dev build-essential
sudo pip install -r requirements.txt

sudo cp ./netclient.service /etc/init.d/netclient
sudo chmod a+x  /etc/init.d/netclient

sudo systemctl daemon-reload  
sudo systemctl enable netclient
sudo systemctl start netclient
