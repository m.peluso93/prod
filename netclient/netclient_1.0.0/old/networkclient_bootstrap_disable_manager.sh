#!/bin/bash

echo "waiting 1minute"
sleep 1m
echo "stopping NetworkManager"
systemctl stop NetworkManager.service
echo "NetworkManager disabled"

echo "NetworkManager STATUS"
systemctl status NetworkManager.service

echo "ifconfig"
ifconfig

echo "neverending reading dev null"
tail -f /dev/null
