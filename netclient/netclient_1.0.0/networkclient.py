#!/usr/bin/python
import os
import sys
import threading
import time
from pyshare import PyShare, PyShareProducer
from sniffer import Sniffer
from time import gmtime, strftime
import subprocess as sub



test = False
if len(sys.argv)>1:
  test = sys.argv[1]=="TEST_MODE"

class NetworkDataProducer(PyShareProducer):
  def __init__(self, test=False):
    self.find_devices()
    self.scanDevices = self.validate_device()
    self.sniffer = Sniffer(self.scanDevices, self.__push__, test)
    self.running = False
    pass

  def find_devices(self):
    """
    """
    os.system("ifconfig | grep -E 'wext|wlan*' > interfaces.txt")
    self.devices = []
    with open('interfaces.txt', 'r') as f:
        for line in f:
            self.devices.append(line.strip().split()[0])
    pass

  def validate_device(self):
    """
    """
    scanDevices = []
    for device in self.devices:
        try :
            ipcmd = "ip link set %s down" % device
            out   = sub.check_output(ipcmd, shell=True)
            scanDevices.append(device)
        except Exception as e:
            print  e
    return scanDevices

  def close(self):
    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print "Closing NetworkDataProducer"
    with threading.Lock() as lock:
      self.sniffer.stop()
      self.running = False
    pass

  def start(self):
    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print "Init NetworkDataProducer"
    with threading.Lock() as lock:
        if not self.running:
          self.running = True
          #Reboot the card and set monitor mode
          self.sniffer.start()
          print "Started NetworkClient"
          print self.sniffer.thread.is_alive()
          pass

    #obj = {"ma": "00:00:00:00:00:", "si": "", "ss": -128, "st": 4, "pr": 1}
    #Since this object extends PyShareProducer we can access the queue like this
    #self.__push__(obj)

os.system("mkdir -p /opt/netclient/tmp/")

#Create a new pyshare object to handle 'netdata' in a queue located at './tmp/queue'

t = time.time()
share = PyShare("netdata", "/opt/netclient/tmp/netdata.queue_" + str(t))

# if os.path.isfile(share.deviceidlocation+"scandevice"):
#     with open(share.deviceidlocation+"scandevice", 'r') as file:
#         scanDevice = file.read().split("\n")[0]

#Define the data, use SQLITE datatypes
#tm and id are automatically injected by pyshare
share.addFieldToDefinition("ma", "TEXT")
share.addFieldToDefinition("si", "TEXT")
share.addFieldToDefinition("ss", "INTEGER")
share.addFieldToDefinition("st", "INTEGER")
share.addFieldToDefinition("pr", "INTEGER")
share.addCreationIndexClause("CREATE INDEX if not exists bucketMa ON pyshare_netdata (ma, tm);")

#TTL in minutes, after this expires without getting any data the producer is re-booted
share.setTTL(1)

#Webservice url, where to store data (absolute path will not be edit anyway)
#http://in.mmma.cloud/ingest/:dataType/:deviceId/
#:deviceId/ will be injected automatically
share.setWebservice("http://in-test.iottacle.com/ingest/netdata/")
#share.setWebservice("http://192.168.178.46:8080/ingest/netdata/")
#Set the sqlite db name
share.setDb("/opt/netclient/tmp/netdata.sqlite")

#Set the producer object
share.setProducerFunction(NetworkDataProducer(test=test))

#Only for testing:
#means run 5 iterations, do not set this parameter to let the program run forever
if test:
    share.run = 5

#Start PyShare
share.__run__()

#If something fails close it
share.close()
