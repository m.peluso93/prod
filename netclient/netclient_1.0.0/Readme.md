# Network scanner - client

## Testing on local machine

Dependencies:

*   python
*   python-pip
*   python-dev
*   libpcap-dev
*   build-essential

Then from pip repository

*   pip install -r requirements.txt

All together

    sudo apt-get install python python-pip python-dev libpcap-dev build-essential
    sudo pip install pyzmq pypcap dpkt

Download submodules

    git submodule update --init --recursive

### Test only package sniffing

You'll need to set env variable 'SCAN_DEVICE' to the name of your wifi device (ie. 'wlan0')

Then you can start the application in 'TEST_MODE'

    cd PRJ_DIR/netclient
    sudo python main.py TEST_MODE


## With Docker (Experimental)

Dependencies

* docker-engine

Building the image

    docker build -t netclient .

Running the image in TEST_MODE on wlan0

    docker run \
	--name netclient \
	-d \
	-e "SCAN_DEVICE=wlan0" \
	-v /dev/wlan0:/dev/wlan0
	--privileged \
        netclient TEST_MODE


## TODO

1. Finire porting a python3
2. Aggiungere un secondo device wifi
3. Automatizzare la scelta dello scanning device
        idea: iw dev | grep interface e fare il parsing
        iserire poi tutto il ciclo run all'interno di un while -- condizione legata al device
4. Creare versione production ready con solo wifi
