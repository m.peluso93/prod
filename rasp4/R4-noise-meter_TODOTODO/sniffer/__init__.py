import os
import time
import threading
from threading import Thread
import subprocess as sub
import datetime
import random

import os, errno
import pyaudio
import spl_lib as spl
from scipy.signal import lfilter
import numpy
import time

''' The following is similar to a basic CD quality
   When CHUNK size is 4096 it routinely throws an IOError.
   When it is set to 8192 it doesn't.
   IOError happens due to the small CHUNK size

   What is CHUNK? Let's say CHUNK = 4096
   math.pow(2, 12) => RATE / CHUNK = 100ms = 0.1 sec
'''
CHUNKS = [4096, 9600]       # Use what you need
CHUNK = CHUNKS[1]
FORMAT = pyaudio.paInt16    # 16 bit
CHANNEL = 1    # 1 means mono. If stereo, put 2

'''
Different mics have different rates.
For example, Logitech HD 720p has rate 48000Hz
'''
RATES = [44300, 48000]
RATE = RATES[1]
#RATE = 44100
NUMERATOR, DENOMINATOR = spl.A_weighting(RATE)

'''
Listen to mic
'''
pa = pyaudio.PyAudio()

sampleRate = pa.get_device_info_by_index(0)['defaultSampleRate']
print('samprerate default')
print(sampleRate)

stream = pa.open(format = FORMAT,
                channels = CHANNEL,
                rate = RATE,
                input = True,
                frames_per_buffer = CHUNK)


class Sniffer:
    def __init__(self, scanDevice, pushFunction, test):
        self.scanDevice=scanDevice
        self.pushFunction=pushFunction
        self.stopSignal=False
        self.test = test
        self.thread = None
        pass

    def start(self):
        self.stopSignal=False
        self.thread = Thread(target=self.__run__)
        self.thread.start()
        pass

    def stop(self):
        with threading.Lock() as lock:
            self.stopSignal=True
            self.thread.join()
        pass

    def is_meaningful(self, old, new):
        return True #abs(old - new) > 3

    def listen(self, old=0, error_count=0, min_decibel=100, max_decibel=0):
        print("Listening")
        while True:
            try:
                ## read() returns string. You need to decode it into an array later.
                block = stream.read(CHUNK, exception_on_overflow=False)
                ## Int16 is a numpy data type which is Integer (-32768 to 32767)
                ## If you put Int8 or Int32, the result numbers will be ridiculous
                decoded_block = numpy.fromstring(block, 'Int16')
                ## This is where you apply A-weighted filter
                y = lfilter(NUMERATOR, DENOMINATOR, decoded_block)
                new_decibel = 20*numpy.log10(spl.rms_flat(y))
                if self.is_meaningful(old, new_decibel):
                    old = new_decibel
                    res = {
                       "db": new_decibel,
                       "ma": "aa:bb:cc:dd:ee:ff",
                       "v": "1"
                    }
                    #print("result decibel: {}".format(new_decibel))
                    self.pushFunction(res)
                    print('A-weighted: {:+.2f} dB'.format(new_decibel))
                else:
                    print("value too low ", new_decibel)
            except Exception as e:
                error_count += 1
                print(" (%d) Error recording: %s" % (error_count, e))

            with threading.Lock() as lock:
                if self.stopSignal:
                    print ("Received stop-signal")
                    break

            time.sleep(1)

        print('stopping')
        stream.stop_stream()
        stream.close()
        pa.terminate()


    def __run__(self):
        print ("Starting dump on: ", self.scanDevice)
        #cmd = './ibeacon_scan -i ' + self.scanDevice + ' -b'
        #cmd = '$HOME/.conda/envs/spl-meter/bin/python spl_meter_text.py'
        #cmd = '/home/pi/.conda/envs/spl-meter/bin/python spl_meter_text.py'
        #print ("COMMAND: " + cmd)
        #pc = sub.Popen(cmd, shell=True, stdout=sub.PIPE)
        print ("Start listening noise...")
        self.listen()
