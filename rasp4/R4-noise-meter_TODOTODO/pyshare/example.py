#!/usr/bin/python

from pyshare import PyShare
from pyshare import PyShareProducer

#Test producer of fake data
class MyProducer(PyShareProducer):
    thread = None
    def close(self):
        print "fake producer closure"

    def start(self):
        print "fake producer start"
        obj = {"ma": "00:00:00:00:00:", "si": "", "ss": -128, "st": 4, "pr": 1}
        for i in range(0, 10):
            #Adding some white noise to the random data
            obj["ma"]="00:00:00:00:00:"
            if i<10:
                obj["ma"]+="0"
            obj["ma"]+=str(i)
            
            #Since this object extends PyShareProducer we can access the queue like this
            self.__push__(obj)

#Create a new pyshare object to handle 'netdata' in a queue located at './tmp/queue'
share = PyShare("netdata", "./tmp/queue")

#Define the data, use SQLITE datatypes
#tm and id are automatically injected by pyshare
share.addFieldToDefinition("ma", "TEXT")
share.addFieldToDefinition("si", "TEXT")
share.addFieldToDefinition("ss", "INTEGER")
share.addFieldToDefinition("st", "INTEGER")
share.addFieldToDefinition("pr", "INTEGER")

#Set the device name and id
#the device name is used only for notifications
share.setDeviceId("TestDevice", "-1000")

#TTL in minutes, after this expires without getting any data the producer is re-booted
share.setTTL(1)

#Webservice url, where to store data (absolute path will not be edit anyway)
#http://in.mmma.cloud/ingest/:dataType/:deviceId/
#:deviceId/ will be injected automatically
share.setWebservice("http://in.mmma.cloud/ingest/netdata/")

#Set the sqlite db name
share.setDb("./tmp/netdata.sqlite")

#Set the producer object
share.setProducerFunction(MyProducer())

print share

#Only for testing:
#means run 5 iterations, do not set this parameter to let the program run forever
share.run = 5

#Start PyShare
share.__run__()

#Only for testing
share.__db_delete__([1, 2, 3])
share.close()
