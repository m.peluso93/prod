#!/bin/bash

echo 'check for connection'
wget --spider http://google.com 2>&1   # primo check
if [ $? -eq 0 ]; then
    echo 'connection present 1'
else
  echo 'missed 1'
  sleep 10
  wget --spider http://google.com 2>&1  # secondo check connessione
  if [ $? -eq 0 ]; then
    echo 'connection present 2'
  else
    echo 'missed 2'
    sleep 10
    wget --spider http://google.com 2>&1   # terzo check connessione
    if [ $? -eq 0 ]; then
      echo 'connection present 3'
    else
      echo 'reboot'
      curl -X POST --header "Content-Type:application/json" \
              "$BALENA_SUPERVISOR_ADDRESS/v1/reboot?apikey=$BALENA_SUPERVISOR_API_KEY"
fi
fi
fi
