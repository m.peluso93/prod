#!/bin/bash

echo 'initialize cronjob'
crontab -r

(echo "*/1 * * * * /bin/bash /opt/pinger/pinger.sh")   | crontab -
(crontab -l; echo "15 4 10 16 22 * * * /bin/bash /opt/pinger/rebooter.sh") | crontab -
(crontab -l; echo "*/1 * * * * /bin/bash /opt/pinger/troubleshoot.sh") | crontab -
(crontab -l; echo "*/10 * * * * /bin/bash /opt/pinger/connection.sh") | crontab -

crond -f
