import requests
import subprocess as sb
import time
import datetime
import os

# -----
os.environ["DBUS_SYSTEM_BUS_ADDRESS"] = "unix:path=/host/run/dbus/system_bus_socket"
deviceId = os.getenv("DEVICE_ID", -1000)
timeAt = datetime.datetime.now()

# check for active wifi connection
try:
    wifiConns = sb.check_output(['nmcli connection show --active | grep wifi'], shell=True).splitlines()
    for wifiConn in wifiConns:
        detailWifi = sb.check_output(['nmcli device wifi'], shell=True)
        connectionName = str(wifiConn.split()[0].decode("utf-8"))
        if connectionName in str(detailWifi.splitlines()[1]):
            # if a wifi is present check its Mbits rate
            try:
                index = detailWifi.splitlines()[1].split().index('Mbit/s')
                rateWifi = int(detailWifi.splitlines()[1].split()[index - 1])

            except ValueError as e:
                rateWifi = 0
            print("{} Device {} Wifi {} Rate {} ".format(timeAt, deviceId, connectionName, rateWifi))
            if rateWifi == 0:
                print("{} Device {} deleting the wifi connection  ".format(timeAt, deviceId,connectionName))
                sb.check_output(["nmcli connection delete {}".format(connectionName)], shell=True)

except sb.CalledProcessError as e:
    print("{} Device {} no active wifi connection".format(timeAt, deviceId))
    wifiConns = ""
connWifiFlag = True if len(wifiConns) > 1 else False

# check for active gsm connection
try:
    connGsm = str(sb.check_output(['nmcli connection show --active | grep gsm'], shell=True))
except sb.CalledProcessError as e:
    print("{} Device {} no active gsm connection ".format(timeAt, deviceId))
    connGsm = ""
connGsmFlag = True if len(connGsm) > 1 else False

# check for active internet connection
try:
    connInternet = str(sb.check_output(["wget --spider http://google.com 2>&1"], shell=True))
except sb.CalledProcessError as e:
    print("{} Device {} no active internet connection ".format(timeAt, deviceId))
    connInternet = ""

connIntFlag = True if 'Connecting' in connInternet else False

if connInternet is False:
    # reboot
    time.sleep(15)
    connInternet = str(sb.check_output(["wget --spider http://google.com 2>&1"], shell=True))
    connIntFlag = True if 'Connecting' in connInternet else False
    if connIntFlag is False:
        time.sleep(15)
        connInternet = str(sb.check_output(["wget --spider http://google.com 2>&1"], shell=True))
        connIntFlag = True if 'Connecting' in connInternet else False
        if connIntFlag is False:
            BALENA_SUPERVISOR_ADDRESS = os.getenv("BALENA_SUPERVISOR_ADDRESS",None)
            BALENA_SUPERVISOR_API_KEY = os.getenv("BALENA_SUPERVISOR_API_KEY",None)
            headers = {'content-type': 'application/json'}
            url = "{}/v1/reboot?apikey={}".format(BALENA_SUPERVISOR_ADDRESS, BALENA_SUPERVISOR_API_KEY)
            print('{} Device {} rebooting {}'.format(timeAt, deviceId, url))
            if BALENA_SUPERVISOR_ADDRESS is not None:
                requests.post(headers=headers,url=url)




