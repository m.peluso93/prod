import requests
import subprocess as sb
import time
import datetime
import os
import json

# -----
os.environ["DBUS_SYSTEM_BUS_ADDRESS"] = "unix:path=/host/run/dbus/system_bus_socket"
deviceId = os.getenv("DEVICE_ID", -1000)
timeAt = datetime.datetime.now()

try:
    checkModem = sb.check_output(["nmcli d | grep -E 'cellular' | grep -w connected"], shell=True)
    checkModemFlag = True
except sb.CalledProcessError as e:
    print("{} Device {} no active cellular connection".format(timeAt, deviceId))
    checkModemFlag = False

try:
    checkWifi = "$(nmcli d | grep -E 'wint*|wlan*' | grep -w connected)"
    checkWifiFlag = True
except sb.CalledProcessError as e:
    print("{} Device {} no active cellular connection".format(timeAt, deviceId))
    checkWifiFlag = False

if checkModemFlag:
    nameModem = sb.check_output(["mmcli -m 0  | grep -E 'primary port: ' | awk 'NF>1{print $NF}'"], shell=True)
    imeiModem = sb.check_output(["mmcli -m 0 | grep imei | awk 'NF>1{print $NF}'"], shell=True)
    stateModem = sb.check_output(["mmcli -m 0 | grep -w '  state' | awk 'NF>1{print $NF}'"], shell=True)
    accesstechModem = sb.check_output(["mmcli -m 0 | grep -w 'access tech' | awk 'NF>1{print $NF}'"], shell=True)
    signalModem = sb.check_output(["""mmcli -m 0 | grep 'signal quality'  | awk 'NF=4' | awk 'NF>1{print $NF}' | tr "'" " " """], shell=True)
    powerstateModem = sb.check_output(["mmcli -m 0 | grep -w 'power state' | awk 'NF>1{print $NF}'"], shell=True)

nameModem =  nameModem.decode("utf-8").replace('\n','').replace("'",'')
imeiModem =  imeiModem.decode("utf-8").replace('\n','').replace("'",'')
stateModem =  stateModem.decode("utf-8")
powerstateModem =  powerstateModem.decode("utf-8").replace('\n','').replace("'",'')
accesstechModem =  accesstechModem.decode("utf-8").replace('\n','').replace("'",'')

signalModem =  signalModem.decode("utf-8").replace('\n','').replace("'",'')
index = signalModem.find('%')
signalModem = signalModem[:index]

cpuUsage = sb.check_output(
    [""" top -b -d1 -n1 | grep -E  'CPU:|usr' | head -n 1 | awk 'NF=4' | awk 'NF>1{print $NF}' | tr "'" " " """],
    shell=True)

cpuUsage = cpuUsage.decode("utf-8").replace('\n','')
index = cpuUsage.find('%')
cpuUsage = cpuUsage[:index]

timestamp = int(time.time() * 1000)
temp = sb.check_output(["/opt/vc/bin/vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*'"], shell=True)


message = {
    "command": "TROUBLESHOOT",
    "tm": timestamp,
    "commandId": "TROUBLESHOOT",
    "deviceId": deviceId,
    "BALENA_DEVICE_UUID": os.getenv("BALENA_DEVICE_UUID"),
    "BALENA_APP_NAME": os.getenv("BALENA_APP_NAME"),
    "App_ID": os.getenv("BALENA_APP_ID"),
    "conncellular": checkWifiFlag,
    "conn_wifi": checkWifiFlag,
    "modemdevice": nameModem,
    "imei": imeiModem,
    "state": stateModem,
    "power_state": powerstateModem,
    "access_tech": accesstechModem,
    "signal_strenght": signalModem,
    "cpu_usage": str(cpuUsage),
    "temp": float(temp.replace(b'\n',b'')),
    "id": 42}

headers = {
    'Content-type': 'application/json',
}
url = "http://in.iottacle.com/ingest/devicestatus-data/{}".format(deviceId)
res = requests.post(url, headers=headers, data="[{}]".format(json.dumps(message)))
print("{} Device {} response ".format(deviceId,timeAt) + str(res))