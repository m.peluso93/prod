#!/bin/bash

export DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

function check_output() {
  if [ -z "$1" ]
  then
     echo "false" # output empty
  else
     echo "true"  # output not empy
  fi
                }

declare -i cpusage
declare -i signal

checkmodem="$(nmcli d | grep -E 'cellular' | grep -w connected)"
checkwifi="$(nmcli d | grep -E 'wint*|wlan*' | grep -w connected)"

flagcellular=$(check_output $checkmodem )
flagwifi=$(check_output $checkwifi )

modemdevice="$(mmcli -m 0  | grep -E 'primary port: ' | awk 'NF>1{print $NF}')"
imei="$(mmcli -m 0 | grep imei | awk 'NF>1{print $NF}')"
state="$(mmcli -m 0 | grep -w '  state' | awk 'NF>1{print $NF}')"
accesstech="$(mmcli -m 0 | grep -w 'access tech' | awk 'NF>1{print $NF}')"
signal="$(mmcli -m 0 | grep "signal quality"  | awk 'NF=4' | awk 'NF>1{print $NF}' | tr "'" " ")"
powerstate="$(mmcli -m 0 | grep -w 'power state' | awk 'NF>1{print $NF}')"
topcmd="$(top -b -n 1 | grep root | awk 'NF>10' | awk  'BEGIN{FS="\\(s\\):\\s*";OFS="";q="\x22" }{$1=q$1q;sub(/%.*$/,"%",$2);$2=q$2q; print $1,$2}')"
cpusage="$(top -b -d1 -n1 | grep -E  'CPU:|usr' | head -n 1 | awk 'NF=4' | awk 'NF>1{print $NF}' | tr "%" " ")"
timestamp="$(python /opt/pinger/gettime.py 2>&1)"
temp="$(/opt/vc/bin/vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*')"

message="[{
      \"command\" :           \"TROUBLESHOOT\",
      \"tm\":                 $timestamp,
      \"commandId\" :         \"TROUBLESHOOT\",
      \"deviceId\":           $DEVICE_ID,
      \"BALENA_DEVICE_UUID\": \"$BALENA_DEVICE_UUID\",
      \"BALENA_APP_NAME\":    \"$BALENA_APP_NAME\",
      \"App_ID\":             $BALENA_APP_ID,
      \"conncellular\":       $flagcellular,
      \"conn_wifi\":          $flagwifi,
      \"modemdevice\":        \"$modemdevice\",
      \"imei\":               \"$imei\",
      \"state\":              \"$state\",
      \"power_state\":        \"$powerstate\",
      \"access_tech\":        \"$accesstech\",
      \"signal_strenght\":    $signal,
      \"cpu_usage\":          $cpusage,
      \"temp\":               $temp,
      \"id\": 42    }]"

curl -H "Content-type: application/json" -X POST http://in.iottacle.com/ingest/devicestatus-data/647  -d "[{\"command\" :           \"TROUBLESHOOT\",\"tm\":                 1,
      \"commandId\" :         \"TROUBLESHOOT\",
      \"deviceId\":           647,
      \"BALENA_DEVICE_UUID\": \"1\",
      \"BALENA_APP_NAME\":    \"1\",
      \"App_ID\":             1,
      \"conncellular\":       true,
      \"conn_wifi\":         true,
      \"modemdevice\":        \"1\",
      \"imei\":               \"1\",
      \"state\":              \"on\",
      \"power_state\":        \"on\",
      \"access_tech\":        \"on\",
      \"signal_strenght\":    1,
      \"cpu_usage\":          1,
      \"temp\":               1,
      \"id\": 122    }]"
