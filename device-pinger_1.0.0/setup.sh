#!/bin/bash

apk  update
apk  add --no-cache openssh curl wireless-tools networkmanager modemmanager

crontab -r

(echo "0 4,11,18 * * * /bin/bash /opt/pinger/rebooter.sh")   | crontab -
(crontab -l; echo "*/10 * * * *  python /opt/pinger/troubleshoot.py") | crontab -
(crontab -l; echo "*/10 * * * * python /opt/pinger/connection.py") | crontab -

crond -f
